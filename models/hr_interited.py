# -*- coding: utf-8 -*-

import logging
from odoo import api, fields, models
from odoo import tools, _
from datetime import timedelta

_logger = logging.getLogger(__name__)

class EmployeeInherited(models.Model):
    _inherit = "hr.employee"

    @api.model
    def send_birthday_reminder_mail_template(self):
        # Function checks if employee's birthdays is today minus 1 week
        # If yes, function sends reminder e-mails to all employees, except one who's birthday is coming

        today = fields.Date.from_string(fields.Date.today())
        upcoming_birthday_month_day = (today - timedelta(weeks=1))
        birthday_employees = self.search([
            ('birthday', 'ilike', '%-{}'.format(upcoming_birthday_month_day.strftime('%m-%d')))
        ])
        template = self.env.ref('birthday_reminder.birthday_reminder_mail_template')
        leftover_employees = self.search([('id', 'not in', birthday_employees.ids)])

        for birthday_employee in birthday_employees:
            employees_to_inform = leftover_employees + (birthday_employees - birthday_employee)
            print '---------------------'
            print birthday_employee.name
            print '---------------------'
            for employee_to_inform in employees_to_inform:
                template.write({'email_to': employee_to_inform.work_email})
                template.send_mail(birthday_employee.id)
                print employee_to_inform.name

