# -*- coding: utf-8 -*-

{
    'name': 'Birthday Reminder',
    'version': '10.0.0.1',
    'author': "Donatas Noreika",
    'summary': 'Birthday Reminder',
    'description': """

    This module every day checks if employee's birthdays is today minus 1 week and sends reminder e-mails to all employees, except one who's birthday is coming.
    
    """,
    'depends': ['hr'],
    'category': 'Product',
    'demo': [
    ],
    'data': [
        'views/mail_templates.xml',
        'views/notifications.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}

